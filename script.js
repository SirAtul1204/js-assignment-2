class Person {
  constructor(name, age, salary, sex) {
    this.name = name;
    this.age = age;
    this.salary = salary;
    this.sex = sex;
  }

  static print(person) {
    console.log(person);
  }

  static sort(arr, field, order) {
    let sortedArray = [...arr];

    function partition(low, high) {
      let pivot = sortedArray[high];
      let i = low - 1;
      for (let j = low; j <= high; j++) {
        if (sortedArray[j][`${field}`] < pivot[`${field}`]) {
          i++;
          let t = sortedArray[i];
          sortedArray[i] = sortedArray[j];
          sortedArray[j] = t;
        }
      }
      let t = sortedArray[i + 1];
      sortedArray[i + 1] = sortedArray[high];
      sortedArray[high] = t;
      return i + 1;
    }

    function quickSort(low, high) {
      if (low < high) {
        let pi = partition(low, high);

        quickSort(low, pi - 1);
        quickSort(pi + 1, high);
      }
    }

    quickSort(0, sortedArray.length - 1);
    if (order === "desc") {
      sortedArray.reverse();
    }
    return sortedArray;
  }
}

const persons = [
  new Person("Atul", 21, 20000, "Male"),
  new Person("Aditi", 23, 40000, "Female"),
  new Person("Ayush", 18, 10000, "Male"),
  new Person("Hoshali", 15, 25000, "Female"),
  new Person("Nishant", 34, 7800, "Male"),
  new Person("Apurv", 39, 50000, "Male"),
];

console.log(Person.sort(persons, "name", "asc"));
